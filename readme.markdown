# Admissions
Normalizes information used by the Online App (the Node.js-based Admissions Application)

# Normalizations Supported So Far
## "DESC" Field of "organizations" Collection
* HS --> High School
* Scho[ol] --> School
* Coll[ege] --> College
* Univ[ersity] --> University

