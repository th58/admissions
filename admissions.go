package admissions

import (
    "fmt"
    "strings"
    "strconv"
    "io/ioutil"
    "encoding/json"
    "regexp"
    "bufio"
    "os"
)

type JsonRoot struct {
    Object []Organization `json:"object"`
}

type Organization struct {
    Id Oid `json:"_id"`
    ExtOrgId int `json:"EXT_ORG_ID"`
    Effdt string `json:"EFFDT"`
    EffStatus string `json:"EFF_STATUS"`
    Description string `json:"DESCR"`
    SortName string `json:"OTH_NAME_SORT_SRCH"`
    OrgType string `json:"EXT_ORG_TYPE"`
    SchoolType string `json:"LS_SCHOOL_TYPE"`
    City json.RawMessage `json:"CITY"`
    State json.RawMessage `json:"STATE"`
    Country string `json:"COUNTRY"`
}

type Oid struct {
    Id string `json:"$oid"`
}

//
// Return true if the description is ambiguous and needs human overview
// For example: if there is a lone "U" a human needs to see if that stands for University or if it's someone's name
//
func detectAmbiguity(description string) bool {

    ambigStrings := []string{
        `pa`,
        `ser`,
        `dod`,
        `me`,
        `med`,
        `serv`,
        `amer`,
        `sc`,
        `st`,
        `cc`,
        `crt`,
        `pr`,
        `co`,
        `ac`,
        `elec`,
    }

    regexLines := []string{
        `(\W+[a-z]\W)`,
        `|([a-z]/[a-z])`,
    }

    for _,i := range ambigStrings {
        regexLines = append(regexLines, fmt.Sprintf(`|((^|\W)%s($|\W))`, i))
    }

    ambigRegex := regexp.MustCompile(strings.Join(regexLines, ""))

    return ambigRegex.MatchString(strings.ToLower(description))
}

//
// Return a fixed copy of an organization's description, no changes if nothing is wrong
// For example, "Scho" would be replaced with "School"
//
func fixDescription(description string) string {

    // pad the input with spaces so that it's not necessary to detect the beginning of the line
    result := " " + description + " "

    // replace "/" with " " to make searches simpler
    result = strings.Replace(result, "/", " ", -1)

    replacements := [][2]string{
        {" Coll ", " College "},
        {" Scho ", " School "},
        {" HS ", " High School "},
        {" Hs ", " High School "},
        {" H s ", " High School "},
        {" H S ", " High School "},
        {" Univ ", " University "},
        {" Acad ", " Academy "},
        {" Acdmy ", " Academy "},
        {" Mgmt ", " Management "},
        {" Intrnatl ", " International "},
        {" Chrstn ", " Christian "},
        {" Wrshp ", " Worship "},
        {" Cntr ", " Center "},
        {" Elem ", " Elementary "},
        {" Vly ", " Valley "},
        {" Cmty ", " Community "},
        {" Tchrs ", " Teachers "},
        {" Brkly ", " Berkeley "},
        {" Intl ", " International "},
        {" Sch ", " School "},
        {" Schl ", " School "},
        {" Schol ", " School "},
        {" Schls ", " Schools "},
        {" Perf ", " Performing "},
        {" Sec ", " Secondary "},
        {" Inst ", " Institute "},
        {" Ins ", " Institute "},
        {" Voc ", " Vocational "},
        {" Priv ", " Private "},
        {" Prvt ", " Private "},
        {" Int'l ", " International "},
        {" Hlth ", " Health "},
        {" Lrng ", " Learning "},
        {" Meml ", " Memorial "},
        {" Mem ", " Memorial "},
        {" Ctr ", " Center "},
        {" Centr ", " Center "},
        {" Cnty ", " County "},
        {" Srvs ", " Services "},
        {" Srvc ", " Service "},
        {" Int'L ", " International "},
        {" Crdit ", " Credit "},
        {" Prep ", " Preparatory "},
        {" Cty ", " City "},
        {" Pub ", " Public "},
        {" Chrt ", " Charter "},
        {" Chrtr ", " Charter "},
        {" Mil ", " Military "},
        {" Alt ", " Alternative "},
        {" Engnrng ", " Engineering "},
        {" Rgnl ", " Regional "},
        {" Regl ", " Regional "},
        {" Reg ", " Regional "},
        {" Healt ", " Health "},
        {" Dist ", " District "},
        {" Acd ", " Academy "},
        {" Acrdtd ", " Accredited "},
        {" Centrl ", " Central "},
        {" Asmbly ", " Assembly "},
        {" Educ ", " Education "},
        {" Inst. ", " Institute "},
        {" Nrsng ", " Nursing "},
        {" Nurs ", " Nursing "},
        {" Prjct ", " Project "},
        {" Mntgmry ", " Montgomery "},
        {" Fndtn ", " Foundation "},
        {" Fnd ", " Foundation "},
        {" Fdtn ", " Foundation "},
        {" Prog ", " Program "},
        {" Prg ", " Program "},
        {" Pgm ", " Program "},
        {" Hnrs ", " Honors "},
        {" Tlntd ", " Talented "},
        {" Tlnt ", " Talent "},
        {" Fdn ", " Foundation "},
        {" Ctrs ", " Centers "},
        {" Wstrn ", " Western "},
        {" Srch ", " Search "},
        {" Schlshp ", " Scholarship "},
        {" Fmly ", " Family "},
        {" Childn ", " Children "},
        {" Councl ", " Council "},
        {" Cncl ", " Council "},
        {" Teachr ", " Teacher "},
        {" Hghlnd ", " Highland "},
        {" Gftd ", " Gifted "},
        {" Gspl ", " Gospel "},
        {" Devel ", " Development "},
        {" Eductnl ", " Educational "},
        {" Mnstry ", " Ministry "},
        {" Clg ", " College "},
        {" Untd ", " United "},
        {" Wrld ", " World "},
        {" Grls ", " Girls "},
        {" Frncs ", " Francais "},
        {" For Deaf ", " For the Deaf "},
        {" School Deaf ", " School for the Deaf "},
        {" Fllrtn ", " Fullerton "},
        {" Clearwtr ", " Clearwater "},
        {" San Fran ", " San Francisco "},
        {" Pg ", " Program "},
        {" Los Ang ", " Los Angeles "},
        {" Of Ed ", " Of Education "},
        {" School Music ", " School of Music "},
        {" Sci ", " Science "},
        {" Orang ", " Orange "},
        {" Univ-Univ ", " University "},
        {" Upwrdbd ", " Upward Bound "},
        {" Upwrd Bnd ", " Upward Bound "},
        {" Upward Bnd ", " Upward Bound "},
        {" Upwrd Bn ", " Upward Bound "},
        {" Upwd Bnd ", " Upward Bound "},
        {" Miamisbrg ", " Miamisburg "},
        {" Rptg ", " Reporting "},
        {" Yth ", " Youth "},
        {" Trnton ", " Trenton "},
        {" Spgs ", " Springs "},
        {" Tech ", " Technology "},
        {" Dept ", " Department "},
        {" Northwstrn ", " Northwestern "},
    }

    // if any of these words are incomplete, they should be completed ("Arizo" should be "Arizona")
    //
    // states excluded because they don't complete well:
    // Idaho, Iowa, Maine, Ohio, Texas, Utah, North Carolina, North Dakota, South Carolina, South Dakota, West Virginia, Indiana, Virginia, Louisiana, Rhode Island
    var wordsToComplete = []string{"Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Illinois", "Kansas", "Kentucky", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "Oklahoma", "Oregon", "Pennsylvania", "Tennessee", "Vermont", "Washington", "Wisconsin", "Wyoming", "College", "School", "University", "Academy", "Technology", "Institute", "Association", "Language", "Education"}

    // append all valid shortened versions of the long words to complete to the list of replacements to perform
    for _,i := range wordsToComplete {
        for l:=5; l<len(i); l++ {
            replacements = append(replacements, [2]string{" " + i[:l] + " ", " " + i + " "})
        }
    }

    // append lowercase versions of every entry to the end of the slice of replacements to perform
    var lowerReplacements [][2]string
    for _,i := range replacements {
        lowerReplacements = append(lowerReplacements, [2]string{strings.ToLower(i[0]), strings.ToLower(i[1])})
    }
    replacements = append(replacements, lowerReplacements...)

    // carry out all replacement operations, one at a time
    for _,i := range replacements {
        result = strings.Replace(result, i[0], i[1], -1)
    }

    // remove the spaces at the beginning and end that were used as padding
    return result[1:len(result)-1]
}

//
// Read the specified file into a JSON object
//
func ReadJson(fileName string) (JsonRoot, error) {
    file, err := ioutil.ReadFile(fileName)
    if err != nil {
        return JsonRoot{}, err
    }

    var vals JsonRoot
    err = json.Unmarshal(file, &vals)
    if err != nil {
        return JsonRoot{}, err
    }

    return vals, nil
}

//
// Print out all organization names that should be fixed, lines alternating like this:
// OLD: Prt Dvr Composite School
// NEW: Port Dover Composite School
//
// Last line contains count of organizations listed
//
func PrintFixList(vals JsonRoot) {
    fixedCount := 0
    for _,v := range vals.Object {
        fixedDesc := fixDescription(v.Description)
        if fixedDesc != v.Description {
            fixedCount++
            fmt.Println("OLD: " + v.Description)
            fmt.Println("NEW: " + fixedDesc + "     id: " + v.Id.Id)
            fmt.Println()
        }
    }
    fmt.Println("Number of entries to fix: " + strconv.Itoa(fixedCount))
}

//
// Change the specified file in palce to make it a valid json file, assuming it is the output of a mongo find() command
// It is possible to use ReadJson() on that file after this function is run
//
func PrepFile(fileName string) error {
    file, err := os.Open(fileName)
    if err != nil {
        return err
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)

    var allLines []string
    allLines = append(allLines, "{\"object\": [")
    for scanner.Scan() {
        allLines = append(allLines, scanner.Text() + ",")
    }
    allLines[len(allLines)-1] = allLines[len(allLines)-1][:len(allLines[len(allLines)-1])-1]
    allLines = append(allLines, "]}")

    toWrite := []byte(strings.Join(allLines, "\n"))
    err = ioutil.WriteFile(fileName, toWrite, 0777)
    if err != nil {
        return err
    }

    return nil
}

//
// Prints a file that, if run in Mongodb with "load(fileName)", will make the fixes
//
func PrintMongoQueries(vals JsonRoot) {
    for _,v := range vals.Object {
        fixedDesc := fixDescription(v.Description)
        if fixedDesc != v.Description {
            fmt.Printf("db.organizations.update({\"_id\": ObjectId(\"%s\")}, {$set: {\"_meta.desc\": \"%s\"}});\n", v.Id.Id, fixedDesc)
        }

        if detectAmbiguity(fixedDesc) {
            fmt.Printf("db.organizations.update({\"_id\": ObjectId(\"%s\")}, {$set: {\"_meta.ambiguous\": true}});\n", v.Id.Id)
        }
    }
}

//
// Prints an overview of numbers of interest
//
func PrintStats(vals JsonRoot) {
    fixedCount := 0
    for _,v := range vals.Object {
        fixedDesc := fixDescription(v.Description)
        if fixedDesc != v.Description {
            fixedCount++
        }
    }
    fmt.Println("Number of entries to fix: " + strconv.Itoa(fixedCount))
}

