package main

import (
    "os"
    "fmt"
    "flag"
    "bitbucket.org/th58/admissions"
)

//
// Panic if there is an error
//
func check(err error) {
    if err != nil {
        fmt.Printf("%s\n", err)
        panic(err)
    }
}

func main() {

    // Make sure there are enough args
    if len(os.Args) < 2 {
        fmt.Println("Usage: admissions [-prep] [-stats] [-list] [-mongo] target.json")
        return
    }

    // Parse command line arguments
    prepPtr := flag.String("prep", "", "Rewrite query results in place to make them into a JSON object")
    statsPtr := flag.String("stats", "", "Print an overview with numbers of interest")
    listPtr := flag.String("list", "", "List out every item that has changed and what it will be changed to")
    mongoPtr := flag.String("mongo", "", "Print out a list of mongo commands that will make the necessary changes")
    flag.Parse()

    if *prepPtr != "" {
        admissions.PrepFile(*prepPtr)
    } else if *statsPtr != "" {
        vals, err := admissions.ReadJson(*statsPtr)
        check(err)
        admissions.PrintStats(vals)
    } else if *listPtr != "" {
        vals, err := admissions.ReadJson(*listPtr)
        check(err)
        admissions.PrintFixList(vals)
    } else if *mongoPtr != "" {
        vals, err := admissions.ReadJson(*mongoPtr)
        check(err)
        admissions.PrintMongoQueries(vals)
    }
}

